package my.bookstore;

import my.bookstore.model.*;

import javax.print.attribute.standard.OrientationRequested;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;

public class Main {

    static ArrayList<Book> books = new ArrayList<>();
    static ArrayList<Customer> customers = new ArrayList<>();
    static ArrayList<Employee> employees = new ArrayList<>();
    static ArrayList<Order> orders = new ArrayList<>();

    public static void main(String[] args) {
        // write your code here
        initData();
        // задача№1
        String booksInfo = String.format("ОБщее количество проданных книг %d на сумму %f", getCountOfSoldBooks(), getAllPriceOfSoldBooks());
        System.out.println(booksInfo);

        // задача №2
        for (Employee employee : employees) {
            System.out.println(employee.getName() + " продал(а)" + getProfitByEmployee(employee.getId()));
        }
        // задача №3
        ArrayList<BookAdditional> soldBooksCount = getCountOfSoldBooksByGenre();
        HashMap<BookGenre, Double> soldBooksPrices = getPriceOfSoldBooksByGenre();

        String soldBookStr = "По жанру: %s продано %d книг общей стоимостью %f";

        for (BookAdditional bookAdditional : soldBooksCount) {
            double price = soldBooksPrices.get(bookAdditional.getGenre());
            System.out.println(
                    String.format(
                            soldBookStr,
                            bookAdditional.getGenre().name(), bookAdditional.getCount(), price));
        }

        // задача №4
        int age = 30;
        String analyzeGenreStr ="Покупатели до %d лет выбирают жанр %s";
        System.out.println(String.format(analyzeGenreStr,30,getMostPopularGenreLessThenAge(30)));

        // задача №4
        String analyzeGenreStr2 ="Покупатели старше %d лет выбирают жанр %s";
        System.out.println(String.format(analyzeGenreStr2,30,getMostPopularGenreMoreThenAge(30)));

    }

    // наиболее популярный жанр по возрасту lj age
    public static BookGenre getMostPopularGenreLessThenAge(int age) {
        ArrayList<Long> customerIds = new ArrayList<>();
        for (Customer customer: customers) {
            if (customer.getAge() < age) {
                customerIds.add(customer.getId());
            }
        }

        return getMostPopularBookGenre(customerIds);
    }

    // наиболее популярный жанр по возрасту старше age
    public static BookGenre getMostPopularGenreMoreThenAge(int age) {
        ArrayList<Long> customerIds = new ArrayList<>();
        for (Customer customer: customers) {
            if (customer.getAge() > age) {
                customerIds.add(customer.getId());
            }
        }

        return getMostPopularBookGenre(customerIds);
    }

    private static BookGenre getMostPopularBookGenre(ArrayList<Long> customerIds) {
        int countArt = 0, countPr =0, countPs = 0;

        for (Order order:orders) {
            if (customerIds.contains(order.getCustomerId())) {
                countArt += getCountOfSoldByGenre(order, BookGenre.Art);
                countPr += getCountOfSoldByGenre(order, BookGenre.Programming);
                countPs += getCountOfSoldByGenre(order, BookGenre.Physhology);
            };
        }
        ;
        ArrayList<BookAdditional> result = new ArrayList<>();
        result.add(new BookAdditional(BookGenre.Art, countArt));
        result.add(new BookAdditional(BookGenre.Programming, countPr));
        result.add(new BookAdditional(BookGenre.Physhology, countPs));

        result.sort(new Comparator<BookAdditional>() {
            @Override
            public int compare(BookAdditional left, BookAdditional right) {
                return right.getCount()-left.getCount();
            }
        });
        return result.get(0).getGenre();
    }

    ;

    // количестов книг по жанру
    public static ArrayList<BookAdditional> getCountOfSoldBooksByGenre() {
        ArrayList<BookAdditional> result = new ArrayList<>();
        int countArt = 0;
        int countPr = 0;
        int countPs = 0;

        for (Order order : orders) {
            countArt += getCountOfSoldByGenre(order, BookGenre.Art);
            countPr += getCountOfSoldByGenre(order, BookGenre.Programming);
            countPs += getCountOfSoldByGenre(order, BookGenre.Physhology);
        }
        result.add(new BookAdditional(BookGenre.Art, countArt));
        result.add(new BookAdditional(BookGenre.Programming, countPr));
        result.add(new BookAdditional(BookGenre.Physhology, countPs));
        return result;
    }


    // получить количество книг в одном заказе по жанру
    public static int getCountOfSoldByGenre(Order order, BookGenre genre) {
        int count = 0;

        for (long bookId : order.getBooks()) {
            Book book = getBookById(bookId);
            if (book != null && book.getGenre() == genre) {
                count++;
            }
        }
        return count;
    }

    // получить общую сумму книг в одном заказе по жанру
    public static double getPriceOfSoldByGenre(Order order, BookGenre genre) {
        double price = 0;

        for (long bookId : order.getBooks()) {
            Book book = getBookById(bookId);
            if (book != null && book.getGenre() == genre)
                price += book.getPrice();
        }
        return price;
    }


    // получить стоимость проданных книг по жанрам
    public static HashMap<BookGenre, Double> getPriceOfSoldBooksByGenre() {
        HashMap<BookGenre, Double> result = new HashMap<>();

        double priceArt = 0;
        double pricePr = 0;
        double pricePs = 0;

        for (Order order : orders) {
            priceArt += getPriceOfSoldByGenre(order, BookGenre.Art);
            pricePr += getPriceOfSoldByGenre(order, BookGenre.Programming);
            pricePs += getPriceOfSoldByGenre(order, BookGenre.Physhology);
        }

        result.put(BookGenre.Art, priceArt);
        result.put(BookGenre.Programming, pricePr);
        result.put(BookGenre.Physhology, pricePs);
        return result;
    }


    /**
     * Получить общее количество и стоимость проанных книг продавцом id
     *
     * @param employeeId
     * @return
     */
    public static Profit getProfitByEmployee(long employeeId) {
        int count = 0;
        double price = 0;
        for (Order order : orders) {
            if (order.getEmployeeId() == employeeId) {
                price += getPriceOfSoldBooksInOrder(order);
                count += order.getBooks().length;
            }
        }
        return new Profit(count, price);
    }


    /**
     * @param id - номер книги
     * @return - возвращает найденную книгу
     */

    // get book by Id
    public static Book getBookById(long id) {
        Book current = null;

        for (Book book : books) {
            if (book.getId() == id) {
                current = book;
                break;
            }
        }

        return current;
    }

    // получить все проданные книги
    public static int getCountOfSoldBooks() {
        int count = 0;

        for (Order order : orders) {
            count = count + order.getBooks().length;
        }
        return count;
    }

    /**
     * получить общую стоимость одного заказа
     *
     * @param order
     * @return
     */
    public static double getPriceOfSoldBooksInOrder(Order order) {
        double price = 0;

        for (long bookId : order.getBooks()) {
            Book book = getBookById(bookId);
            if (book != null)
                price += book.getPrice();
        }
        return price;
    }

    // получить общую сумму заказов
    public static double getAllPriceOfSoldBooks() {
        double price = 0;
        for (Order order : orders) {
            price += getPriceOfSoldBooksInOrder(order);
        }

        return price;
    }


    public static void initData() {
        employees.add(new Employee(1, "Maria Ivanova", 37));
        employees.add(new Employee(2, "Ivan Ivanov", 47));
        employees.add(new Employee(3, "Serg Ivanov", 57));

        customers.add(new Customer(1, "Sidorov Alex", 25));
        customers.add(new Customer(2, "Romanov Ivan", 32));
        customers.add(new Customer(3, "Simovon Eldar", 18));

        books.add(new Book(1, "Financist", "Teodor Draizer", 1600, BookGenre.Art));
        books.add(new Book(2, "War and Piece", "Tolstoy Lec", 1500, BookGenre.Art));

        books.add(new Book(3, "Dead soul", "Dostoevsky Fedor", 1300, BookGenre.Art));
        books.add(new Book(4, "Man and woman", "Fraid Zigmond", 1200, BookGenre.Physhology));
        books.add(new Book(5, "Manipulation and actualization", "Everett Shostorm", 1150, BookGenre.Physhology));
        books.add(new Book(6, "C++ start", "Zinich Roman", 1100, BookGenre.Programming));

        orders.add(new Order(1, 1, 1, new long[]{1, 2, 3}));
        orders.add(new Order(2, 2, 2, new long[]{2, 3, 4}));
        orders.add(new Order(3, 3, 3, new long[]{4, 5, 6}));
    }
}
